# currency-convertor
This is a simple currency converter which is based on https://exchangeratesapi.io/ 

**Task description**

Create a site backed by a RESTful API which receives it will get three inputs:

1) A source currency
2) A target currency
3) A monetary value

The API must leverage the exchange rates provided at https://exchangeratesapi.io/ and leverage that to return a converted value. So if your input as 30, USD and GBP, you would need to return the calculated result.

Expose your logic behind a RESTful API
1) The API has to be written in Java. Feel free to use a framework of your choice.
2) Develop the API as you would be developing actual production software - leverage validation, testing, caching
3) We want to see and test your API, so make the code available to us together with the link to the Git repository

Not to make this open to interpretation, treat as bad input everything that’s not in the list of supported currencies on the site.

Extra Points: Deploy your application to a hosted service such as Heroku or AWS

**Solution description**

Here is a one dockerized service which go via adapter to https://exchangeratesapi.io/

Technologies:
1) Docker, docker-compose
2) EhCache
3) Swagger

**How to use**
1) Go to root directory
2) Build project

   _mvn clean install_
3) Build and start docker-compose

   _docker-compose up_
4) Go to currency-converter swagger
   http://localhost:2010/currency-converter/swagger-ui.html
