package com.buralek.currencyconvertor.exception.handler;

import com.buralek.currencyconvertor.exception.CantFindTargetCurrencyException;
import com.buralek.currencyconvertor.exception.ExchangeRatesApiIntegrationException;
import com.buralek.currencyconvertor.exception.dto.ErrorResponse;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class ServiceExceptionHandler extends ResponseEntityExceptionHandler {
    private final static String INTERNAL_SOURCE = "internal";
    private final static String EXCHANGE_RATE_API_SOURCE = "exchange-rate-api";

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(final HttpMessageNotReadableException exception, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
        final String errorMessage = "Can't parse request";
        final ErrorResponse response = new ErrorResponse(new Date(), HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase(), INTERNAL_SOURCE, errorMessage, exception.getMessage());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public ResponseEntity<ErrorResponse> ExchangeRatesApiIntegrationExceptionHandler (final ExchangeRatesApiIntegrationException exception) {
        final String errorMessage = "Integration error";
        final ErrorResponse response = new ErrorResponse(new Date(), HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(), EXCHANGE_RATE_API_SOURCE, errorMessage, exception.getMessage());
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler
    public ResponseEntity<ErrorResponse> CantFindTargetCurrencyExceptionHandler (final CantFindTargetCurrencyException exception) {
        final ErrorResponse response = new ErrorResponse(new Date(), HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(), EXCHANGE_RATE_API_SOURCE, exception.getMessage(), exception.getMessage());
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
