package com.buralek.currencyconvertor.exception;

public class ExchangeRatesApiIntegrationException extends RuntimeException {
    public ExchangeRatesApiIntegrationException(String message) {
        super(message);
    }
}
