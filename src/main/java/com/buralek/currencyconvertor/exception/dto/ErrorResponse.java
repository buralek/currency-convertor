package com.buralek.currencyconvertor.exception.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@AllArgsConstructor
public class ErrorResponse {
    private Date timestamp;
    private Integer status;
    private String statusText;
    private String source;
    private String message;
    private String debugMessage;
}
