package com.buralek.currencyconvertor.exception;

public class CantFindTargetCurrencyException extends RuntimeException {
    public CantFindTargetCurrencyException(String message) {
        super(message);
    }
}
