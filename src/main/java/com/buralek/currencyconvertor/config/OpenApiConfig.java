package com.buralek.currencyconvertor.config;


import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.context.annotation.Configuration;

@Configuration
@OpenAPIDefinition(info = @Info(title = "Currency convertor project", version = "${service-settings.version}"))
public class OpenApiConfig {
}
