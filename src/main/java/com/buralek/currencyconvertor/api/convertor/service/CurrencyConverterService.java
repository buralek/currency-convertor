package com.buralek.currencyconvertor.api.convertor.service;

import com.buralek.currencyconvertor.api.convertor.dto.CurrencyConvertRequest;
import com.buralek.currencyconvertor.api.convertor.dto.CurrencyConvertResponse;

public interface CurrencyConverterService {
    CurrencyConvertResponse convert(CurrencyConvertRequest request);
}
