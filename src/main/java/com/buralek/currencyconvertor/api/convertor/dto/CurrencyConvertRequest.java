package com.buralek.currencyconvertor.api.convertor.dto;

import lombok.Data;

@Data
public class CurrencyConvertRequest {
    private String sourceCurrency;
    private String targetCurrency;
    private Double value;
}
