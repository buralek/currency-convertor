package com.buralek.currencyconvertor.api.convertor.service;

import com.buralek.currencyconvertor.api.convertor.dto.CurrencyConvertRequest;
import com.buralek.currencyconvertor.api.convertor.dto.CurrencyConvertResponse;
import com.buralek.currencyconvertor.exception.CantFindTargetCurrencyException;
import com.buralek.currencyconvertor.service.exchangeratesapi.adapter.ExchangeRatesApiAdapter;
import com.buralek.currencyconvertor.service.exchangeratesapi.dto.ExchangeRatesLatestResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class CurrencyConverterServiceImpl implements CurrencyConverterService {
    private final ExchangeRatesApiAdapter exchangeRatesApiAdapter;

    @Override
    public CurrencyConvertResponse convert(final CurrencyConvertRequest request) {
        final ExchangeRatesLatestResponse exchangeRatesLatestResponse = exchangeRatesApiAdapter.getLatest(request.getSourceCurrency());
        final Double targetRate = exchangeRatesLatestResponse.getRates().get(request.getTargetCurrency());
        if (targetRate == null) {
            throw new CantFindTargetCurrencyException("Can't find target currency for " + request.getTargetCurrency());
        }
        final CurrencyConvertResponse response = new CurrencyConvertResponse();
        response.setValue(targetRate * request.getValue());
        return response;
    }
}
