package com.buralek.currencyconvertor.api.convertor.dto;

import lombok.Data;

@Data
public class CurrencyConvertResponse {
    private Double value;
}
