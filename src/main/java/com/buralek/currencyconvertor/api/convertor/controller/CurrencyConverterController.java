package com.buralek.currencyconvertor.api.convertor.controller;

import com.buralek.currencyconvertor.api.convertor.dto.CurrencyConvertRequest;
import com.buralek.currencyconvertor.api.convertor.dto.CurrencyConvertResponse;
import com.buralek.currencyconvertor.api.convertor.service.CurrencyConverterService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping("convert")
@RequiredArgsConstructor
public class CurrencyConverterController {
    private final CurrencyConverterService currencyConverterService;

    @PostMapping
    @Operation(summary = "Get connection detail by id")
    public ResponseEntity<CurrencyConvertResponse> convert(@RequestBody final CurrencyConvertRequest request) {
        log.info("Start converting process for request {}'", request);
        final CurrencyConvertResponse response = currencyConverterService.convert(request);
        log.info("Converter response is {}'", response);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
