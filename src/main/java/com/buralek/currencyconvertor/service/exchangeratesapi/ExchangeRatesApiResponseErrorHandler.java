package com.buralek.currencyconvertor.service.exchangeratesapi;

import com.buralek.currencyconvertor.exception.ExchangeRatesApiIntegrationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

import static org.springframework.http.HttpStatus.Series.CLIENT_ERROR;
import static org.springframework.http.HttpStatus.Series.SERVER_ERROR;

@Component
@Slf4j
public class ExchangeRatesApiResponseErrorHandler implements ResponseErrorHandler {
    @Override
    public boolean hasError(final ClientHttpResponse httpResponse) throws IOException {
        return (httpResponse.getStatusCode().series() == CLIENT_ERROR
                || httpResponse.getStatusCode().series() == SERVER_ERROR);
    }

    @Override
    public void handleError(final ClientHttpResponse clientHttpResponse) throws IOException {
        final String responseAsString = toString(clientHttpResponse.getBody());
        log.error("Exchange Rates Api integration exception: '{}'", responseAsString);
        throw new ExchangeRatesApiIntegrationException("Exchange Rates Api integration exception: " + responseAsString);
    }

    private String toString(InputStream inputStream) throws IOException {
        // I parse error message like a string, because I'm not sure that error message get at the same format for each error case
        // If I had a guarantee about error messages from this service I would use ObjectMapper and parse this json into specific Error class
        Scanner s = new Scanner(inputStream).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }
}
