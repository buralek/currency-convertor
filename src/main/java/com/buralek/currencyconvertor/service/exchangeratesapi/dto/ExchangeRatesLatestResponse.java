package com.buralek.currencyconvertor.service.exchangeratesapi.dto;

import lombok.Data;

import java.time.LocalDate;
import java.util.Map;

@Data
public class ExchangeRatesLatestResponse {
    private Map<String, Double> rates;
    private String base;
    private LocalDate date;
}
