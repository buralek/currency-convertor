package com.buralek.currencyconvertor.service.exchangeratesapi.adapter;

import com.buralek.currencyconvertor.service.exchangeratesapi.dto.ExchangeRatesLatestResponse;

public interface ExchangeRatesApiAdapter {
    ExchangeRatesLatestResponse getLatest(String base);
}
