package com.buralek.currencyconvertor.service.exchangeratesapi.adapter;

import com.buralek.currencyconvertor.service.exchangeratesapi.ExchangeRatesApiResponseErrorHandler;
import com.buralek.currencyconvertor.service.exchangeratesapi.dto.ExchangeRatesLatestResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Service
@Slf4j
public class ExchangeRatesApiAdapterImpl implements ExchangeRatesApiAdapter {
    private final String LATEST_PATH = "https://api.exchangeratesapi.io/latest";
    private final String GET_CACHE_SPEL = "@cacheManager.getCache('exchangeratesapi_latest').get(#base, T(com.buralek.currencyconvertor.service.exchangeratesapi.dto.ExchangeRatesLatestResponse))";

    private final RestTemplate restTemplate;

    public ExchangeRatesApiAdapterImpl(final RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder
                .errorHandler(new ExchangeRatesApiResponseErrorHandler())
                .build();
    }

    @Override
    @Cacheable(value = "exchangeratesapi_latest",
            key = "#base",
            condition = GET_CACHE_SPEL + " == null " +
                    "|| !T(java.time.LocalDate).now().equals(" + GET_CACHE_SPEL + ".getDate())"
            )
    public ExchangeRatesLatestResponse getLatest(final String base) {
        log.info("Get latest rate for '{}'", base);
        final UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(LATEST_PATH)
                .queryParam("base", base);
        return restTemplate.getForEntity(
                builder.toUriString(),
                ExchangeRatesLatestResponse.class).getBody();
    }
}
