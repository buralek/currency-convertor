package com.buralek.currencyconvertor.api.convertor.controller;

import com.buralek.currencyconvertor.api.convertor.dto.CurrencyConvertRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CurrencyConverterController.class)
public class CurrencyConverterControllerTests {
    @MockBean
    private CurrencyConverterController currencyConverterController;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void convertTest() throws Exception {
        final ObjectMapper objectMapper = new ObjectMapper();
        final String request = objectMapper.writeValueAsString(new CurrencyConvertRequest());
        this.mockMvc.perform(post("/convert").contentType(MediaType.APPLICATION_JSON).content(request))
                .andDo(print())
                .andExpect(status().isOk());
    }
}
