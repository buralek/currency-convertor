package com.buralek.currencyconvertor.api.convertor.service;

import com.buralek.currencyconvertor.api.convertor.dto.CurrencyConvertRequest;
import com.buralek.currencyconvertor.api.convertor.dto.CurrencyConvertResponse;
import com.buralek.currencyconvertor.exception.CantFindTargetCurrencyException;
import com.buralek.currencyconvertor.service.exchangeratesapi.adapter.ExchangeRatesApiAdapter;
import com.buralek.currencyconvertor.service.exchangeratesapi.dto.ExchangeRatesLatestResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@SpringBootTest
public class CurrencyConverterServiceTests {
    private final String USD = "USD";
    private final String RUB = "RUB";
    private final Double RATE = 73.735034918;
    private final Double VALUE = 1000.0;

    @Autowired
    private CurrencyConverterService connectionDetailsService;

    @MockBean
    private ExchangeRatesApiAdapter exchangeratesapiAdapter;

    @Test
    void convertCorrectTest() {
        when(exchangeratesapiAdapter.getLatest(USD)).thenReturn(createCorrectResponse());
        final CurrencyConvertResponse response = connectionDetailsService.convert(createRequest());
        final Double checkValue = VALUE * RATE;
        assertEquals(response.getValue(), checkValue);
    }

    @Test
    void convertIncorrectTest() {
        when(exchangeratesapiAdapter.getLatest(USD)).thenReturn(createIncorrectResponse());
        assertThrows(CantFindTargetCurrencyException.class, ()-> connectionDetailsService.convert(createRequest()));
    }

    private CurrencyConvertRequest createRequest() {
        final CurrencyConvertRequest currencyConvertRequest = new CurrencyConvertRequest();
        currencyConvertRequest.setSourceCurrency(USD);
        currencyConvertRequest.setTargetCurrency(RUB);
        currencyConvertRequest.setValue(1000.0);
        return currencyConvertRequest;
    }

    private ExchangeRatesLatestResponse createCorrectResponse() {
        final ExchangeRatesLatestResponse response = new ExchangeRatesLatestResponse();
        response.setBase(USD);
        final Map<String, Double> rates = new HashMap<>();
        rates.put(RUB, 73.735034918);
        response.setRates(rates);
        return response;
    }

    private ExchangeRatesLatestResponse createIncorrectResponse() {
        final ExchangeRatesLatestResponse response = new ExchangeRatesLatestResponse();
        response.setBase(USD);
        final Map<String, Double> rates = new HashMap<>();
        response.setRates(rates);
        return response;
    }
}
